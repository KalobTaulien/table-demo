<h2>Pure PHP</h2>
<p>This code look cleaner, and lets you write easier comments</p>
<hr />
<?php 


	if(isset($_GET['name']) && isset($_GET['email'])) {

		// Welcome the user
		echo "<h2>";
			echo "Welcome, " . htmlspecialchars($_GET['name']); // Escaped for security
			echo " <small>(" . htmlspecialchars($_GET['email']) . ")</small>";  // Escaped for security
		echo "</h2>";

		// Start table
		echo "<table width='600' border='1' cellspacing='1' cellpadding='1'>";
			for($i = 1; $i <= 25; $i++) {
				// If this is the very first row, add the <tr>
				if($i == 1) {
					echo "<tr>";
				}

				// Echo the cell and the number
				echo "<td>" . $i . "</td>";

				// If the cell number divided by 5 has a remainder of 0, create a new row.
				if($i % 5 == 0) {
					echo "<tr>";

					// If this is NOT the very last row, start another row
					if($i != 25) {
						echo "<tr>";
					}
				}
			}
		// End table
		echo "</table>";

		?>
		<script>
			setTimeout(function() {
				alert("Hi <?php echo $_GET['name']; ?>. Welcome to my game!");
			}, 5000);			
		</script>
		<?php
	} else {
		// Ask the user for their name and email address.
		?>

		<h2>Please enter your name and email address.</h2>
		<form method='GET'>
			<label for="name">Your Name</label>
			<input type="text" name='name' required='required'>
			<br />

			<label for="email">Email Address</label>
			<input type="email" name="email" required='required'>
			<br />

			<button type="submit">Start Game!</button>
		</form>

		<?php
	}
?>

